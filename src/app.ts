import base64url from 'base64url';
import chalk from 'chalk';
import crypto from 'crypto';
import randomstring from 'randomstring';
import { JWK, JWS, JWE } from 'jose';

export const KEY_SPCP_PRIVATE =
    'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb2dJQkFBS0NBUUVBNjk2dzlldmMydXBKVmVlZ2Zqb0tIMGIxZWpyd0YweUNPcG9UMm9IU09xSTFnd3I5CkVyRlBaZDY1TC9nNHpjNURyYkdkbjhnUkNMZnovS3k3ejVXRHhoZXlSc2gxL1RwK2J6NHgrV1dhNDVnMkhmS0IKTGJPbUFXRXlBMUdwcGI5KzZxazh5WWsxckxpbmVEeWNGWi9maGdyM2FYb0tPcjVpdGFNK21nQUZ3eTFjRXNnTgphZmxsbUs0M25PK2JLRUFSZlJkM0FMSkdYKzlHY2d3cWJtQ1RkZzMyU0IrN2QvVWhLM0YrR2d5dlY4dUQ4OERPCndqUk96UmFKa01lZldNVXVtUUdTNXU1eUNUT0hDRmRmeFdNUDJqZFdkdnhYdUhrWStEOEVIcW52N1h1cGloVUsKeXlScWpFenBzYWlLcFJZcllObkw1RkhhdUNoS2dIRWpzOVFiMHdJREFRQUJBb0lCQUJFRUo5QVdEQURmRmh0VQpjb2VvdUVJNVpFeEZKRDBLRC9zYVVvUlpDVW1obWlxeUNEL28rc2xtb0FXTjZzc3VMa1BCcEpWNllZQTNoU3FkCmE0ODJRaU53WWpRcEVob0t0a1JnUUVxMXdjN1psRTNreU56N01PYTRsRnNZOFJ3OERIUHhjYWs0ZE43WkUzdmgKSTRHTDN4eFVmalRsMFFjYjVSTWxpOGFnYWNvQ3dFU2hVU0JVSnpsd1puYXRaSnJHQlEyTnh4VHNpVDNXaXNragpUa2JRWDkvU1dCVlkwQVhlQ0JZQ3FBaWVsUVZLMXR3NDNLTVRrMzNWeDNJM3pVdVJNWGVhMnA0SWhIRXJzVzBwCm1WalMvc252bXBlQnZrbXhCZ0xzaGxpR3J4VXZRV2o3cFZQamw0a0d6d2FYeEUwTTN4WWdTUlhKSVM5S0EzVnEKc1czMWpNRUNnWUVBK0VvS0NyeCt6OFlPdFlVcGI2c3JibHlYK3FobjVIYmw0R0p0NDhxQWU3cEQ3Z0R4cmtabgpBTndGV0pHWVlPZ08xODJuUUtPWWdKb1VQTGtmZERRTExPRHhFLzl6SFlNOUZqdWZzU241Zi9WeHUydlUzcm5MCnZQU0dyNDJCbnNJRTNnSlJPa2FlaU5YbEYraTFueXBLZzlEaVBndjd4UnZCemVVd29PZjlVSjhDZ1lFQTh6SHEKVUk3ZkdvQlhtN01TT3FQc1hNTElBWnozdjFoZklhTnVPZmF1eUhReWJoYWQ4MFp6MEdPaDl3SFFzcmlFSXVMNQpkRm1CNUtXNmlzajRWSXQzNUtkZXh6SFE5bTl1U1ZYZDRaYlllUm9qVzJxbUUzTE5iZjVpZnI1ZURQUWI2QnN6Ck9xc0dXSlVrUGpkNG5HSUtyQ28xR3lrMzBaVGRCbUlzcGRqanBFMENnWUE1VGkxZktQNUR5anllM0tzaG9Pb1gKUUx1N0dKOVI5YmM4d0xzSU5qYk5WOEpSN2xuWmxWbzBzcE54UzdsVnRIbW9pVWl1L0pNNEsxZThaQ1hFVmphMgpGdnpOVnNvazU2Tnp2RE14V3JUQk9jOFVLT0VxdnNzdFEycktuZWhxMGZENXBLTkd4Z0hWSnprRythamhvWXN0ClBVcmFjejJhMHJzNllFbUcrY1JYbFFLQmdINnBMNlYzVStHRW1KeGlIb3RtRnRPVkt3QVVGY0NWc1M1ZTVTalIKUHVUdUI5bm9jNmxodHk0QXhqbFV1N0pwZ1lCK0FIaEZxZTVCRnEvenpjcVN2UVhjMmZNYkVmOVh0L1FBQWNqZApmSzlPR3I0aEJKQTZpWjNSRU5GOEtwWEtpY0pRY0ZVTkFBZ2czVGgrUVpZSlJnKzNCK08yaXZqZjlHRy92Z3MrCnRBazlBb0dBS2p4SEw0RFMrUnhMVXd2dXM5ZW54Y29GTjIrLzBjVU9MTCtDdnJ6Skszc3lmbkRpa1duOTZYejgKT1JhQTBzZDdINDQxMWhKK3pmaTRpWndLcy9hZ08zUTdUZGlLNVZkYlNYWGZPTzFmdThBTTNoOTk5OG9vZmtvSQpWSk5kcXlKS0ErWXY3RTRYSXI1WFNYNmdFWkIrZEt6dUdUZnpOY1BiTWU5ODVCNmdUT2M9Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg==';
export const KEY_SPCP_PUBLIC =
    'LS0tLS1CRUdJTiBSU0EgUFVCTElDIEtFWS0tLS0tCk1JSUJDZ0tDQVFFQTY5Nnc5ZXZjMnVwSlZlZWdmam9LSDBiMWVqcndGMHlDT3BvVDJvSFNPcUkxZ3dyOUVyRlAKWmQ2NUwvZzR6YzVEcmJHZG44Z1JDTGZ6L0t5N3o1V0R4aGV5UnNoMS9UcCtiejR4K1dXYTQ1ZzJIZktCTGJPbQpBV0V5QTFHcHBiOSs2cWs4eVlrMXJMaW5lRHljRlovZmhncjNhWG9LT3I1aXRhTSttZ0FGd3kxY0VzZ05hZmxsCm1LNDNuTytiS0VBUmZSZDNBTEpHWCs5R2Nnd3FibUNUZGczMlNCKzdkL1VoSzNGK0dneXZWOHVEODhET3dqUk8KelJhSmtNZWZXTVV1bVFHUzV1NXlDVE9IQ0ZkZnhXTVAyamRXZHZ4WHVIa1krRDhFSHFudjdYdXBpaFVLeXlScQpqRXpwc2FpS3BSWXJZTm5MNUZIYXVDaEtnSEVqczlRYjB3SURBUUFCCi0tLS0tRU5EIFJTQSBQVUJMSUMgS0VZLS0tLS0K';
export const KEY_RP_PRIVATE =
    'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlKS0FJQkFBS0NBZ0VBcDhlZm5lUlpoRzVlbG52eDdkc1B3dEJWS0hwQ1ZiNG9jenRzVkpzR0FLWlFXNWNZCkRQZDdKM01ueW9TK2RyZXowTWtmbkNVcFpZbnl3NmZPQTIrdFJmb2RMOHVxOWRpWk84ZzVQZEFZcUNSRW1ocEUKUXZIaGN0ekhLeDd3akozcDBLZkRHK2g2VloxQ2ZkYS8vSjBuUnZIQ0hoSUkrZk1lWlBSeGNuOWNRWC85V0pxcwpjbWFxRk5xNjA1VGduZFo5aC9CRm8vQng1NmFjdGVnZ05rbSt3OTFzSVZHaGJ0SnVnZ1FDNTFrMFh3SElSaHBFCkNmUVEyRld5SXVtdE02T2Z2VGF1QzNxSXFOalpVTmhlejVBY2ZoRnpHWHgrRndUZ0tLTmphbUZtQldtWm11eVUKbG5Lc1IxUVhEeGlTWnBxcCtIdEpiYW1VL0lyazIxM3hwUEdjYzJFL295bEdWcDEvOTdIQy9oZE4xUlF0a2NvYgpVZHRXZlBxV1BZSllWRU03dURmUFgyZGs3alVhWVFQS3FiK0NSeFlZV2dJUTF6VlUrVERkSEJ4NUV3TG0ybWhDCnpVQzhnUEpjd3FXNGtOMit0eVg0U3dUQXBEc2VKYkp1b0VURnIxMnJMVlNCTno5TkFDcFNSL0owcVBROEVXVnkKRTRUUFIzSG1XNURJenZQM3FvZEUxRjVrc254RVBzTnVrdmdCNElOUkRuS1BGUXdFeWg4R0lqVWpLYmNpK3labwptZW5WZUxTTUg5MnA4U0grbkZOL0VIa2dnT1pBYXg3azV4bG84Tm4vOGdaV1BFcHpiS2pWUWVyMDlsclZYV2NKCnRhYkkxZW40SkZ2WFlUUjBhL05ZTEtpazNSY1NnRUUxQ3YvbjNQMFEzTHd4K3N5RGwzUGpFRTdaSXljQ0F3RUEKQVFLQ0FnQUExRmZSckFOVlNFWm5jQTF1STVzY1YzTlVVK1pvRGh5ZEZUb2Y0UFJTcEgvWk5CSUdYbUxaZXhSRApTQXk1QVlkK0RNQi9RUi9IUkx0U2hFM2NibnpvejZlOVNNQjducXFEUlNZalRCZ1RITHJLR0Q4bUpaUDd3V09xCnJ1aDVpWDVTYmNIbExXME5aMDJnMmFMeFdNV004RDBKY1FWaVZ2VmJnbW5kS3d3RnR1QkZ3OTMrWXN1TWR0aGQKbTJyUTNQS1o3WElxc1ZHZTE1QUd4ak1Da2JMRHZTQi91YXVXcEt1dk1OM2l6dW9YWGlialVkWHNScXllcFMzUQpSbDk0MWVKQVNUanJEWndIV2FzWlh6aDR4am1kMWZBL3ZBSFRrMDZZWk5aay9ENUk0SkRGMTAyVXdOUmhOZVUzClNlUFBaZ3haRWlMMnk5UFV1OHNOMnBVTFA3YUtTVy96V0d3bU43TnIzaHlsM0IvNnhhN25VZXhMY2VNSjhrZkcKMmM3SzNOYmxxNnBDTzIxYVJOSE5HbHVVbFlsRjBPVktIcENEdWJsK2xKRXpJeWN6T2RzWkt0VnRXTTFNMnBnOQozS2t6d25TQ2VXSW1XbkVDdGYvWmdhNm1rSEJYNHhHNkxCNkJTMzBMZ3ZTN2NHZ3ZGVngvUHhIUmtpMEtEdUczCkR5dkdySm9QV24vSGNWN3BVMVRxeWRuRllVL200amJQQUZBWktBYUhiSzA1dndsVVhiMk5KQ29kVm9sMHN5dm0KZUUzNXBCWEZPU3hDeWtqd2NyeFRoakdpSy9ZZkswenpWMDJLd2RmYTlrbUNLTlNNSEpHUUFieEo4ZXZYaU9vUAp3MDU0OUZqVS80WC9RNXprVmF1eUI3anpQcHQ0UFNFYlFpNGZKQjNHbnFzcnZXWjJBUUtDQVFFQTFiU2RoOS8zCkNPc3cyYXhJa0I4b2ZaUi83bnFLZFg5ZWZpQ01hQzdCVG9yQy9WYlFyMU0ybDVTVjErS3pDWDVvZUhpV2xaRWIKWHBES2xuay9MRGl5VWdnY05SU2JCMWxmaGZ6Z2hJWWhaMklrMmtXdklVd2ZQTGlFdnluUlFyQnhrQWlLM1I4Sgp3dGlDOWZvVXNDRUpwR3V3eTNaRllGWSsxUTlkOWdpa1RWdmVNWjVxdjQ0dDNacnZaUjUzdTFnaWVCcHUzK2xSClBzcG5TQ0RJaEVGZnEvS0xkd3I0RGkzKzFlWFZyVDJpVW9MNWdvWmx6WDBwbkM1bmVKdnZrUis3WWVBME9RNmwKeEVyMyt4TEthRU1ZN3RWN2dIQ0dBZjNKSVZTRmxDMnFybTcrZXlUbjd5Skd5NURseW9vcTFXTUd6MVVnQkxUVwpZZlhZL21ZZnB0QTdad0tDQVFFQXlQd3ViQklkVkJPSmxVK2V5MEtDTUNOV3ZGUFlveFoxOEFsWFd6d0IzUzFrCisxRWg3aUF0LzNGUHlXUVBITmdpYnV4a3VXNGZOSjRENDBGVXdpaGYyMGZQb3VpRnk5YkNzUW93YmNuT2pFVzIKdWRKc0E1QU5yVnlKbmMrczlrM2ZjOTJ6K0cyenovQ0oySDlrN1VxTElMenZLb1pjWG5Xd0xtYlcrbnhXbVdMeAowWmc4ZHcxR3pFdjdBamR1SWwyOFh6V3B2NXNmVzdMclhraVNYM0I3eENTdGlnY2NtanlmVE1yck5tUnpHOHpHCmFxbXUrdDA3R2cwUmRmYUJWWHgydHoxQ1Y2VEdhczlUWVlrNW5RQWlFaTdWMEQzSXd2eDNwSnpEbi9pRTQyeXAKUFBIYktBRU1YbklKaW0ra2QyTDdvWnZUWjJMUWVhRzBvRkh6WFpyQ1FRS0NBUUFOQ0pmMituelppV2FRVUFhWQo2NTJQa3lmK1JuRDlXNGhLenZkaFAwREE1b3VmNGRTV2JNL09qTnhhcjFQV0IyU2R2RlVLeUMvUUk2cUhaTnVxCmVjYStaQW84WG9aWisyUnRDcEF5NU85U205UzlqelJ3Sm5GZFJhK3E3UVQ3T1VHajFER0w3T0d1aFgvdk9wSmEKYjVsajhzRjBsMEVCOENDaTQrNEtIRnJROFFKcktrcytaWkI4akg5L1BrMTFHcG4za3owaDROdjRwV1dKSzU2dgpHa0dZZmhGdGNEalN2SkZuUUQ0NDg2SmJ2K3N4djdxanZyZHo5QXlSZVlreDQ2cU0vSnlTd2tidjR0YWU5SFZ0CnlISFQxeWRTSFFxL1FsZ3E5VG0wWU5paERObnVic0RLZjkrcmVLbmJJWFFkVVkzUHhWUlBsU3ZpQzVseFlvOFoKb0dlUEFvSUJBRHY5RVRvT2VoU0RORmlOaUQ3dzNmelNQMnc4dG5lUEY2cUtqemxiQXM5cVlpNmsrN1NnaERvdgpMdnBVVkRKdXpIQkJQSnNnSkU1SEw2L2Y2eFY4aVlmZ3VqZk1vVXJERWg5WGhEYnd1aWFpRkRVSWVsYmg0ME13ClVKNzZ3b3hObC9sMFhlZ3pqaXp0aGFkSjl1emdpQUJBaEQ0MEZsUFhXcnV1MUZHMVZQKzVTYVllZzBXaGY5VXcKaEg0WDMxYUFsZDN4SFhaa0xZcm9MMXBYM09DQnFWOVU5enN3ZmpnSENpRVltL1lESzF4TDFaazJobFM0UFpDNwovODh1d1ZHcnNaOWx1R3lDY1BxbVlTUEdtcHlpMG9pTTFDS3dua0NjckhWRW1Sc2hTVUM4U1JNNHVRb0tyZzl1CmlDeDZ3Q01tTnliNFpMUUNDV21VZTE0eXQ2bmpyRUVDZ2dFQkFNa1F3Y09KcjliSmxGeG55Zml4TGIzeFFtYXcKemxuTFdVcUdWVWRaSGZiNzYzdEdzWExleFUyUmQzUEZTV3cxUVdLcXZZanFjQVJ1ak1HZTBJN2NLaG9DZFNZYgp5dktkSFMwYytqSDhFY3lDVGpta0J1Mkcza1lDZHR6VXFvYTVYeFhPRmxSczduM1Q1VlRlcEZDSm4xSnhPbVh4CnFGK2trWTVlVGRweHU0aDl1YkkyT1Fuclh6VnBZd1pUQ0c1a3JTRFZCOXFJN0d2S0oybVhoOTIxRXRHcGI1TjAKWlo3cnB2WmJ6QVR5OXlCZFJKUHdlR296VEdSaGpzekNYa2dDcnBYNW9ZejlYaVBFdWt3dWRUN01ORFJyTytqagpNc3RNZEVyT1dTWi9nVlFXWVJVQnBxTlJORUYyako2M3hnMW1XWUM0Vk92ZmliTk55WEI5ZDFFemhMOD0KLS0tLS1FTkQgUlNBIFBSSVZBVEUgS0VZLS0tLS0K';
export const KEY_RP_PUBLIC =
    'LS0tLS1CRUdJTiBSU0EgUFVCTElDIEtFWS0tLS0tCk1JSUNDZ0tDQWdFQXA4ZWZuZVJaaEc1ZWxudng3ZHNQd3RCVktIcENWYjRvY3p0c1ZKc0dBS1pRVzVjWURQZDcKSjNNbnlvUytkcmV6ME1rZm5DVXBaWW55dzZmT0EyK3RSZm9kTDh1cTlkaVpPOGc1UGRBWXFDUkVtaHBFUXZIaApjdHpIS3g3d2pKM3AwS2ZERytoNlZaMUNmZGEvL0owblJ2SENIaElJK2ZNZVpQUnhjbjljUVgvOVdKcXNjbWFxCkZOcTYwNVRnbmRaOWgvQkZvL0J4NTZhY3RlZ2dOa20rdzkxc0lWR2hidEp1Z2dRQzUxazBYd0hJUmhwRUNmUVEKMkZXeUl1bXRNNk9mdlRhdUMzcUlxTmpaVU5oZXo1QWNmaEZ6R1h4K0Z3VGdLS05qYW1GbUJXbVptdXlVbG5LcwpSMVFYRHhpU1pwcXArSHRKYmFtVS9JcmsyMTN4cFBHY2MyRS9veWxHVnAxLzk3SEMvaGROMVJRdGtjb2JVZHRXCmZQcVdQWUpZVkVNN3VEZlBYMmRrN2pVYVlRUEtxYitDUnhZWVdnSVExelZVK1REZEhCeDVFd0xtMm1oQ3pVQzgKZ1BKY3dxVzRrTjIrdHlYNFN3VEFwRHNlSmJKdW9FVEZyMTJyTFZTQk56OU5BQ3BTUi9KMHFQUThFV1Z5RTRUUApSM0htVzVESXp2UDNxb2RFMUY1a3NueEVQc051a3ZnQjRJTlJEbktQRlF3RXloOEdJalVqS2JjaSt5Wm9tZW5WCmVMU01IOTJwOFNIK25GTi9FSGtnZ09aQWF4N2s1eGxvOE5uLzhnWldQRXB6YktqVlFlcjA5bHJWWFdjSnRhYkkKMWVuNEpGdlhZVFIwYS9OWUxLaWszUmNTZ0VFMUN2L24zUDBRM0x3eCtzeURsM1BqRUU3Wkl5Y0NBd0VBQVE9PQotLS0tLUVORCBSU0EgUFVCTElDIEtFWS0tLS0tCg==';

/**
 * header object and claim object
 */
const claimsObj = { a: 'hello', b: 'world' };
const jwsHeaderObj = {
    alg: 'RS256',
    typ: 'JWT',
};
const jweHeaderObj = {
    enc: 'A128GCM',
    alg: 'RSA-OAEP',
};

// get ready all the keys
const spcpPrivateKey = Buffer.from(KEY_SPCP_PRIVATE, 'base64');
const spcpPublicKey = Buffer.from(KEY_SPCP_PUBLIC, 'base64');
const rpPrivateKey = Buffer.from(KEY_RP_PRIVATE, 'base64');
const rpPublicKey = Buffer.from(KEY_RP_PUBLIC, 'base64');

// alg: RS256 | ES512
const joseEncodeJWS = (payloadObj: any, pemPrivateKeyBase64String: string, joseHeader: any): string => {
    const payloadObjString = JSON.stringify(payloadObj);
    const privateKey = JWK.asKey(Buffer.from(pemPrivateKeyBase64String, 'base64'));
    return JWS.sign(payloadObjString, privateKey, joseHeader);
};

const joseVerifyJWS = (jwsToken: string, pemPublicKeyBase64String: string): any => {
    try {
        const publicKey = JWK.asKey(Buffer.from(pemPublicKeyBase64String, 'base64'));
        return JWS.verify(jwsToken, publicKey);
    } catch (e) {
        console.log(e.message);
    }
    return {};
};

const joseEncodeAndSignJWE = (
    payloadObj: any,
    pemPublicKeyBase64String: string,
    pemPrivateKeyBase64String: string,
    joseHeader: any
): string => {
    const jwsJoseHeader = { alg: 'RS256', typ: 'JWT' };
    const publicKey = JWK.asKey(Buffer.from(pemPublicKeyBase64String, 'base64'));
    const jwsToken = joseEncodeJWS(payloadObj, pemPrivateKeyBase64String, jwsJoseHeader);
    return JWE.encrypt(jwsToken, publicKey, joseHeader || { kid: publicKey.kid });
};

const joseDecodeAndVerifyJWE = (
    jweToken: string,
    pemPublicKeyBase64String: string,
    pemPrivateKeyBase64String: string
): any => {
    const privateKey = JWK.asKey(Buffer.from(pemPrivateKeyBase64String, 'base64'));
    const jwsToken = JWE.decrypt(jweToken, privateKey).toString();
    return joseVerifyJWS(jwsToken, pemPublicKeyBase64String);
};

console.log(`-------------- ${chalk.yellow('start using JWS library')} --------------`);
const jwsToken = joseEncodeJWS(claimsObj, KEY_SPCP_PRIVATE, jwsHeaderObj);
const jwsVerifiedPayload = joseVerifyJWS(jwsToken, KEY_SPCP_PUBLIC);
console.log(jwsToken);
console.log(jwsVerifiedPayload);

const [header, payload, signature] = jwsToken.split('.');
console.log(`${chalk.yellow('header')}: ${header}`);
console.log(`${chalk.yellow('payload')}: ${payload}`);
console.log(`${chalk.yellow('signature')}: ${signature}`);
console.log(`-------------------------------------------------------------------------\n`);

console.log(`-------------- ${chalk.yellow('start manaul JWS creation')} --------------`);
const headerAndPayload = `${base64url(JSON.stringify(jwsHeaderObj))}.${base64url(JSON.stringify(claimsObj))}`;
const signatureFunction = crypto.createSign('SHA256');
signatureFunction.write(`${headerAndPayload}`);
signatureFunction.end();
const manualSignature = base64url(signatureFunction.sign(spcpPrivateKey));
const manualJWS = `${headerAndPayload}.${manualSignature}`;
console.log(`${chalk.yellow('manualJWS')}: ${manualJWS}`);
console.log(`-------------------------------------------------------------------------\n`);

console.log(`-------------- ${chalk.yellow('start manaul JWS verification')} --------------`);
const hash1 = crypto
    .createHash('SHA256')
    .update(`${headerAndPayload}`)
    .digest('base64');
console.log(`${chalk.yellow('hash1')}: ${hash1}`);

const manuallyDecryptedHash = crypto.publicDecrypt(spcpPublicKey, Buffer.from(signature, 'base64'));
console.log(`${chalk.yellow('decryptedHash:')}: ${manuallyDecryptedHash.toString('base64')}`);
const ans1DecodedValue = '78178AAC37CC2DDB8D56DEA91389B542982C42F021202400D90C0BB825B72E2C';
const hash2 = Buffer.from(ans1DecodedValue, 'hex').toString('base64');
console.log(`${chalk.yellow('hash2')}: ${hash2}`);
console.log(`-------------------------------------------------------------------------\n`);

// /* ----------------- comment all above ---------------- */
console.log(`-------------- ${chalk.yellow('start using JWE library')} --------------`);
const jweToken = joseEncodeAndSignJWE(claimsObj, KEY_RP_PUBLIC, KEY_SPCP_PRIVATE, jweHeaderObj);
const jweVerifiedPayload = joseDecodeAndVerifyJWE(jweToken, KEY_SPCP_PUBLIC, KEY_RP_PRIVATE);
console.log(jweToken);
console.log(jweVerifiedPayload);
console.log(`-------------------------------------------------------------------------\n`);

/**
 * | header | encryptedSymKey | iv | encryptedContent | authTag |
 */
console.log(`-------------- ${chalk.yellow('start manaul JWE creation')} --------------`);
const jwsTokenToBeEncrypted = joseEncodeJWS(claimsObj, KEY_SPCP_PRIVATE, jwsHeaderObj);
// 1. header
const jweHeader = JSON.stringify(jweHeaderObj);

// 2. encryptedSymKey
const cek = randomstring.generate(16);
const encryptedCek = crypto.publicEncrypt(rpPublicKey, Buffer.from(cek));

// 3. iv
const iv = randomstring.generate(12);

// 4. encryptedContent
const cipher = crypto.createCipheriv('aes-128-gcm', cek, iv, { authTagLength: 16 });
cipher.setAAD(Buffer.from(base64url(jweHeader)));
const chipherText = Buffer.concat([cipher.update(jwsTokenToBeEncrypted), cipher.final()]);

// 5. authTag
const chipherTextAuthTag = cipher.getAuthTag();

const manualJweToken =
    base64url(jweHeader) +
    '.' +
    base64url(encryptedCek) +
    '.' +
    base64url(iv) +
    '.' +
    base64url(chipherText) +
    '.' +
    base64url(chipherTextAuthTag);
// console.log(encryptedCek);
const manualJweVerifiedPayload = joseDecodeAndVerifyJWE(manualJweToken, KEY_SPCP_PUBLIC, KEY_RP_PRIVATE);
console.log(manualJweVerifiedPayload);
console.log(`-------------------------------------------------------------------------\n`);

console.log(`-------------- ${chalk.yellow('start manaul JWS verification')} --------------`);
const verifyFunction = crypto.createVerify('SHA256');
verifyFunction.write(`${headerAndPayload}`);
verifyFunction.end();
const key = Buffer.from(KEY_SPCP_PUBLIC, 'base64');
console.log(verifyFunction.verify(key, signature, 'base64'));
console.log(`-------------------------------------------------------------------------\n`);
